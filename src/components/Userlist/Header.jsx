import React from 'react'
import './userlist.css'

const Header = () => {
  return (
    <>
      <nav className="navbar navbar-expand-lg bg-body-tertiary fixed-top bg-white navbar-white">
        <div className="container-fluid d-flex justify-content-between align-items-center">
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <a className="navbar-brand" href="#"><img src="assets/images/logo care.jpg" className='logo_care me-5' /></a>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">

            </ul>
            <form className="d-flex" role="search">
              <div className='d-flex justify-content-around align-items-center '>
                <div className='d-flex justify-content-around align-items-center search'>
                  <input className="input-field w-100 mx-3" type="search" placeholder="Quick Search" aria-label="Search" />
                  <button className="btn me-2" type="submit"><i className="fa-solid fa-magnifying-glass"></i></button>
                </div>
                <div className="hstack gap-1">
                  <div className="p-2 admin mx-5 me-0">
                    <img src="assets/images/admin.jpg" className="rounded" alt="admin" />
                  </div>
                  <div className="vr h-75 my-auto"></div>
                  <div className="p-1 d-flex flex-column justify-content-start align-items-start">
                    <span className='admin-name'>Eden Richardson</span>
                    <span className='admin-txt'>Admin</span>
                  </div>
                </div>

                <div className="btn-group mx-5 me-3">
                  <button type="button" className="btn drop-down-btn dropdown-toggle" data-bs-toggle="dropdown" data-bs-display="static" aria-expanded="false">
                    <i className="fa-solid fa-angle-down"></i>
                  </button>
                  <ul className="dropdown-menu dropdown-menu-lg-end my-3">
                    <li><a className="dropdown-item mb-2" href="#">
                      <i className="fa-regular fa-user drop-down-txt"></i>
                      <span className='mx-3 drop-down-txt'>My Profile</span>
                    </a></li>
                    <li><a className="dropdown-item" href="#">
                      <i className="fa-solid fa-arrow-right-from-bracket drop-down-txt"></i>
                      <span className='mx-3 drop-down-txt'>Sign Out</span>
                    </a></li>
                  </ul>
                </div>
              </div>
            </form>
            <div>

            </div>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Header
