import React, { useEffect, useState } from 'react'
import Header from './Header'
import Sidebar from './Sidebar'
import axios from 'axios'

const API = 'https://jsonplaceholder.typicode.com';

const Userlist = () => {
  const [mydata, setMydata] = useState([]);
  const [isError, setIsError] = useState("");

  const getApiData = async(url)=>{
    try {
      const res = await axios.get(url);
      setMydata(res.data);
    } catch (error) {
      setIsError(error.message);
    }
  }

  useEffect(()=>{
    getApiData(`${API}/users`);
  },[])
  return (
    <>
      <Header />
      <Sidebar />
      <div className="container-fluid mt-5 pt-5 ms-5 position-fixed">
        <div className="container mw-100 d-flex justify-content-between align-items-center heading">
          <p className='fw-bold employees'>Employees</p>
          <button className="btn new-employee px-4 me-5 fw-bold" type="button">Add New Employee</button>
        </div>

        <div className='my-4 container-fluid px-5 me-5 table-scroll min-vh-100'>
          <table className="table table-responsive ">
            <thead>
              <tr>
                <th className='col-auto py-2'>Id No.</th>
                <th className='col-auto py-2'>Name</th>
                <th className='col-auto py-2'>Username</th>
                <th className='col-auto py-2'>Email</th>
                <th className='col-auto py-2'>Phone</th>
                <th className='col-auto py-2'>City</th>
                <th className='col-auto py-2'>Company Name</th>
              </tr>
            </thead>
            <tbody>
              {isError !== "" && <h2 className='text-danger fw-bold'>{isError}</h2>}
              {mydata.map((curElem)=>{
                const {id, name, username, email, phone, address, company} = curElem;
                return(
                <tr key={id} >
                <th scope="row" className='py-3'>{id}</th>
                <td className='py-3'>{name}</td>
                <td className='py-3'>{username}</td>
                <td className='py-3'>{email}</td>
                <td className='py-3'>{phone}</td>
                <td className='py-3'>{address.city}</td>
                <td className='py-3'>{company.name}</td>
              </tr>
              )})}
              
            </tbody>
          </table>
        </div>
      </div>

    </>
  )
}

export default Userlist
