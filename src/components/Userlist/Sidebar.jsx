import React, { useState } from 'react'
import './userlist.css'

const Sidebar = () => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    
    return (
        <>
            <div className="container-fluid">
                <div className="row">
                    <div style={{width: isOpen ? "300px" : "70px"}} className="col-auto col-md-2 min-vh-100 sidebar_div position-fixed">
                        <div className="w-100 text-decoration-none d-flex align-items-center justify-content-end ms-3 mt-5 pt-5 text-white">
                            <div className='d-flex align-items-center justify-content-center arrow-right' onClick={toggle}>
                                {isOpen ? <i className="fa-solid fa-angle-left"></i> : <i className="fa-solid fa-angle-right"></i>}
                            </div>
                        </div>
                        <ul className="nav nav-pills flex-column">
                            <li className="nav-item text-white fs-2 pt-2 my-1">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className="bi bi-speedometer"></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>Dashboard</span>
                                </a>
                            </li>
                            <li className="nav-item text-white fs-2 my-1 ">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className="bi bi-people"></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>Employees</span>
                                </a>
                            </li>
                            <li className="nav-item text-white fs-2 my-1 ">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className="bi bi-file-earmark-text"></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>PDP</span>
                                </a>
                            </li>
                            <li className="nav-item text-white fs-2 my-1 ">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className="bi bi-person"></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>Service Users</span>
                                </a>
                            </li>
                            <li className="nav-item text-white fs-2 my-1 ">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className="bi bi-exclamation-triangle"></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>Incidents</span>
                                </a>
                            </li>
                            <li className="nav-item text-white fs-2 my-1 ">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className="bi bi-bar-chart-fill"></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>Performance</span>
                                </a>
                            </li>
                            <li className="nav-item text-white fs-2 my-1 ">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className="bi bi-files"></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>Service User Liasion</span>
                                </a>
                            </li>
                            <li className="nav-item text-white fs-2 my-1 ">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className='bi bi-clock'></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>Rotas</span>
                                </a>
                            </li>
                            <li className="nav-item text-white fs-2 my-1 ">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className="bi bi-send"></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>Competencies</span>
                                </a>
                            </li>
                            <li className="nav-item text-white fs-2 my-1 ">
                                <a href="" className="nav-link text-grey" aria-current="page">
                                    <i className="bi bi-folder2"></i>
                                    <span style={{display: isOpen ? "inline" : "none"}} className='ms-4'>Policies & Procedures</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Sidebar
