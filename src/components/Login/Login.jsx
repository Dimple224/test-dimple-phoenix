import React from 'react'
import './login.css'

const Login = () => {
    return (
        <>
            <div className="container">
                <div className="row w-100 d-flex justify-content-center align-items-center main_div">
                    <div className="col-12 col-md-8 col-xxl-5">
                        <div className="card py-3 px-2">

                            <div className="d-flex justify-content-center align-items-center flex-column">
                                <img src="assets/images/logo.jpg" className='img-fluid' alt="logo" />
                                <hr className='hr' />
                                <h3 className='login-ac mb-4'>Login to your account.</h3>
                            </div>

                            <form className='mx-4'>
                                <fieldset className='border rounded-3 px-5 mb-4'>
                                    <legend className='float-none w-auto px-2'>Email</legend>
                                    <div className="form-group mb-2 d-flex justify-content-between align-items-center">
                                        <i className="fa-regular fa-user pt-1"></i>
                                        <input className="input-field w-100" type="email" />
                                    </div>
                                </fieldset>

                                <fieldset className='border rounded-3 px-5 mb-4'>
                                    <legend className='float-none w-auto px-2'>Password</legend>
                                    <div className="form-group mb-2 d-flex justify-content-between align-items-center">
                                        <img src="assets/icons/password.png" alt="" className='password' />
                                        <input className="input-field w-100" type="password" />
                                    </div>
                                </fieldset>

                                <div className="d-flex justify-content-between my-5">
                                    <div className="form-group mb-4 form-check">
                                        <input type="checkbox" className="form-check-input" />
                                        <label className="form-check-label">Remember Me</label>
                                    </div>
                                    <a href="" className='forgot'><span>forgot password?</span></a>
                                </div>

                                <div className="d-grid">
                                    <button className="btn login" type="button">Login</button>
                                </div>
                            </form>
                            <div className="text-center pt-5 mt-1 d-flex justify-content-center align-items-center copyright">
                                ©2022
                                <span className="text-black mx-1">Care Preference</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Login
